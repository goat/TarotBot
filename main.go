package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/mattn/go-mastodon"
	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Flags: []cli.Flag {
      		&cli.StringFlag{
        		Name: "type",
        		Value: "3 Card Spread",
        		Usage: "Type of Reading to preform. Options are 3 Card Spread and Celtic Cross",
      		},
      		&cli.StringFlag{
      			Name: "source",
      			Value: "time",
      			Usage: "The source of entropy to use",
      		},
      		&cli.StringFlag{
      			Name: "clientid",
      			Value: "",
      			Usage: "The ClientID of the mastodon app",
      			EnvVars: []string{"CLIENT_ID"},
      		},
      		&cli.StringFlag{
      			Name: "clientsecret",
      			Value: "",
      			Usage: "The clientsecret of the mastodon app",
      			EnvVars: []string{"CLIENT_SECRET"},
      		},
      		&cli.StringFlag{
      			Name: "accesstoken",
      			Value: "",
      			Usage: "The Access Token of the mastodon app",
      			EnvVars: []string{"ACCESS_TOKEN"},
      		},
      		&cli.BoolFlag{
      			Name: "post",
      			Value: false,
      			Usage: "Wether to post or not",
      		},
    	},

  		Name: "Tarot",
    	Usage: "Reads Tarot",
    	
    	Action: func(c *cli.Context) error {
    		reading(c)
    		return nil
    	},
  	}

  err := app.Run(os.Args)
  	if err != nil {
    	log.Fatal(err)
  	}
}

func reading(c *cli.Context) {
	deck := initcards()

	found, id := checkForReadingType(c.String("type"))

	if found == false {
		log.Fatal("Could not find Reading Type. Try a different one!")
	}

	fmt.Println("Init Tarot")
	rand.Seed(generateSeed("test"))
	fmt.Printf("Draw Type: %v\n", drawtypes[id].name)
	fmt.Println("\nStarting Shuffle...")

	positions := drawtypes[id].positions
	deck = shuffle(deck, rand.Intn(100))

	fmt.Println("Starting Draw...")
	fmt.Println()
	postid := ""

	//TODO: Fix this so we can actually make the First Post in the thread with info about the draw. Make uploads optional?

	//content := fmt.Sprintf("Tarot Reading for Today. The Draw type is %v",drawtypes[id].name)
	// if c.Bool("post") == true {
	// 	postid = toot(content, "", c.String("clientid"), c.String("clientsecret"), c.String("accesstoken"), "https://hellsite.site", postid)
	// }
	for i := 0; i < drawtypes[id].cardcount; i++ {
		newdeck, drawncard := drawcard(deck)
		deck = newdeck
		output := fmt.Sprintf("Card: %v\nPosition: %v\nMeaning: %v\n\n", drawncard.name, positions[i],drawncard.meaning)
		fmt.Printf(output)
		card := fmt.Sprintf("cards/%v",drawncard.image)
		if c.Bool("post") == true {
			postid = toot(output, card, c.String("clientid"), c.String("clientsecret"), c.String("accesstoken"), "https://hellsite.site", postid)
		}
	}
}

func toot(input string, image string, ClientID string, ClientSecret string, AccessToken string, Instance string, replyid string) (postid string) {
	c := mastodon.NewClient(&mastodon.Config{
		Server: Instance,
		ClientID: ClientID,
		ClientSecret: ClientSecret,
		AccessToken: AccessToken,
	})

	attachment, err := c.UploadMedia(context.Background(),image)

	if err != nil {
		log.Fatal(err)
	}

	toot := mastodon.Toot{
		Status:      input,
		InReplyToID: mastodon.ID(replyid),
		MediaIDs:    []mastodon.ID{attachment.ID},
		Sensitive:   true,
		SpoilerText: "",
		Visibility:  "unlisted",
	}

	status, err := c.PostStatus(context.Background(),&toot)

	if err != nil {
		log.Fatal(err)
	}

	return string(status.ID)
}
func checkForReadingType(readingtype string) (found bool, id int) {
	id = -1
	
	for i, p := range drawtypes {
		if p.name == readingtype {
			found = true
			id = i
		}
	}
	return found, id
}

func drawcard(deck []card) (newdeck []card, drawncard card) {
	//Takes the current deck state, shuffles, draws a card, returns new deck minus the drawn card
	drawncard = deck[0]
	newdeck = deck[1:]
	return newdeck, drawncard
}

func shuffle(deck []card, count int) (shuffled []card) {
	//Implements Fisher-Yates Shuffling
	n := len(deck)
	//Loop count of shuffle int
	for j := 0; j < count; j++ {	
		//actually do Fisher-Yates
		for i := n-1; i > 0; i-- {
			r := rand.Intn(i+1)
			deck[i], deck[r] = deck[r], deck[i]
		}
	}
	return deck
}

func generateSeed(entropySource string) (seed int64) {
	return time.Now().UnixNano()
}

type card struct {
	name string
	meaning string
	image string
}

type patterns struct {
	name string
	purpose string
	cardcount int
	positions []string
}

var drawtypes = []patterns {
	{
		name: "3 Card Spread",
		purpose: "General Fortune Telling",
		cardcount: 3,
		positions: []string{"Past", "Present", "Future"},
	},
	{
		name: "Celtic Cross",
		purpose: "Ten Card spread for further divination",
		cardcount: 10,
		positions: []string{"Present","Challenge","Subconcious","Past","Future","Near Future","Internal Influences","External Influences","Hopes / Fears","Outcome"},

	},
	{
		name: "Vibe Check",
		purpose: "A Single card reading for checking the vibes of a situation",
		cardcount: 1,
		positions: []string{"Vibes"},
	},
}