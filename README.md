# Tarot Bot

## A bot for reading Tarot cards via mastodon or locally

This bot is a tarot reading bot written in Go, mostly for fun. It currently supports 3 different types of draw (Celtic Cross, 3 Card Spread and "Vibe Check"), one source of randomness, one deck of card images and one set of meanings.

Todo:
- Rework meanings for each card
- Seperate out draw types, like how cards.go is setup
- Add more sources of randomness
- Invocations for each draw