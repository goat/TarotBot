package main

func initcards() (deck []card) {
	mj := majorarcana()
	deck = append(deck, mj...)

	wd := wands()
	deck = append(deck, wd...)

	cd := cups()
	deck = append(deck, cd...)

	sd := swords()
	deck = append(deck, sd...)

	pd := pentacles()
	deck = append(deck, pd...)

	return deck
}

func majorarcana() (deck []card) {
	deck = []card {
		{
			name: "The Fool",
			meaning: "Innocence, Free Spirit, New Beginnings",
			image: "fool.jpeg",
		},
		{
			name: "The Magician",
			meaning: "Willpower, Desire, Creation, Manifestation",
			image: "magician.jpeg",
		},
		{
			name: "The High Priestess",
			meaning: "Intuition, Unconcious, Inner Voice",
			image: "highpriestess.jpeg",
		},
		{
			name: "The Empress",
			meaning: "Motherhood, Fertility, Nature",
			image: "empress.jpeg",
		},
		{
			name: "The Emperor",
			meaning: "Authority, Structure, Control, Fatherhood",
			image: "emperor.jpeg",
		},
		{
			name: "The Hierophant",
			meaning: "Tradition, Conformity, Morality, Ethics",
			image: "hierophant.jpeg",
		},
		{
			name: "The Lovers",
			meaning: "Partnerships, Union, Duality, Choice",
			image: "lovers.jpeg",
		},
		{
			name: "The Chariot",
			meaning: "Direction, Control, Willpower",
			image: "chariot.jpeg",
		},
		{
			name: "Strength",
			meaning: "Bravery, Compassion, Focus, Inner Strength",
			image: "strength.jpeg",
		},
		{
			name: "The Hermit",
			meaning: "Contemplation, Truth, Inner Guidance",
			image: "hermit.jpeg",
		},
		{
			name: "The Wheel",
			meaning: "Change, Cycles, Inevitability, Fate",
			image: "wheeloffortune.jpeg",
		},
		{
			name: "Justice",
			meaning: "Cause and Effect, Clarity, Truth",
			image: "justice.jpeg",
		},
		{
			name: "The Hanged Man",
			meaning: "Sacrifice, Release, Betrayl",
			image: "hangedman.jpeg",
		},
		{
			name: "Death",
			meaning: "Endings, Beginnings, Change, Metamorphasis",
			image: "death.jpeg",
		},
		{
			name: "Temperance",
			meaning: "Balence, Patience, Meaning",
			image: "temperance.jpeg",
		},
		{
			name: "The Devil",
			meaning: "Excess, Materialism, Playfulness",
			image: "devil.jpeg",
		},
		{
			name: "The Tower",
			meaning: "Upheavel, Pride, Disaster",
			image: "tower.jpeg",
		},
		{
			name: "The Star",
			meaning: "Hope, Faith, Rejuvenation",
			image: "star.jpeg",
		},
		{
			name: "The Moon",
			meaning: "Unconcious, Illusions, Intuition",
			image: "moon.jpeg",
		},
		{
			name: "The Sun",
			meaning: "Joy, Success, Celebration, Positivity",
			image: "sun.jpeg",
		},
		{
			name: "Judgement",
			meaning: "Reflection, Reckoning, Inner Voice",
			image: "judgement.jpeg",
		},
		{
			name: "The World",
			meaning: "Fufillment, Harmony, Completion",
			image: "world.jpeg",
		},
	}
	return deck
}

func wands() (deck []card) {
	deck = []card {
		{
			name: "Ace of Wands",
			meaning: "Creation, Willpower, Inspiration",
			image: "acewands.jpeg",
		},
		{
			name: "Two of Wands",
			meaning: "Planning, Decision Making, Leaving Home",
			image: "2wands.jpeg",
		},
		{
			name: "Three of Wands",
			meaning: "Looking Ahead, Expansion, Growth",
			image: "3wands.jpeg",
		},
		{
			name: "Four of Wands",
			meaning: "Community, Home, Celebration",
			image: "4wands.jpeg",
		},
		{
			name: "Five of Wands",
			meaning: "Competition, Rivalry, Conflict",
			image: "5wands.jpeg",
		},
		{
			name: "Six of Wands",
			meaning: "Victory, Success, Reward",
			image: "6wands.jpeg",
		},
		{
			name: "Seven of Wands",
			meaning: "Perseverance, Defensiveness, Control",
			image: "7wands.jpeg",
		},
		{
			name: "Eight of Wands",
			meaning: "Rapid Action, Movement, Quick Decisions",
			image: "8wands.jpeg",
		},
		{
			name: "Nine of Wands",
			meaning: "Resiliance, Grit, Determination",
			image: "9wands.jpeg",
		},
		{
			name: "Ten of Wands",
			meaning: "Accomplishment, Responsibility, Burden",
			image: "10wands.jpeg",
		},
		{
			name: "King of Wands",
			meaning: "Leadership, Overcoming Challenges, Planning",
			image: "kingwands.jpeg",
		},
		{
			name: "Knight Of Wands",
			meaning: "Action, Adventure, Fearlessness",
			image: "knightwands.jpeg",
		},
		{
			name: "Page of Wands",
			meaning: "Exploration, Excitement, Freedom",
			image: "pagewands.jpeg",
		},
		{
			name: "Queen of Wands",
			meaning: "Courage, Determination, Joy",
			image: "queenwands.jpeg",
		},
	}
	return deck
}

func cups() (deck []card) {
	deck = []card {
		{
			name: "Ace of Cups",
			meaning: "New Feelings, Spirituality, Intuition",
			image: "acecups.jpeg",
		},
		{
			name: "Two of Cups",
			meaning: "Unity, Partnerships, Connection",
			image: "2cups.jpeg",
		},
		{
			name: "Three of Cups",
			meaning: "Friendship, Community, Happiness",
			image: "3cups.jpeg",
		},
		{
			name: "Four of Cups",
			meaning: "Apathy, Contemplation, Disconnectedness",
			image: "4cups.jpeg",
		},
		{
			name: "Five of Cups",
			meaning: "Loss, Grief, Pity",
			image: "5cups.jpeg",
		},
		{
			name: "Six of Cups",
			meaning: "Familiarity, Happy Memories, Healing",
			image: "6cups.jpeg",
		},
		{
			name: "Seven of Cups",
			meaning: "Searching for Purpose, Choices, Daydreaming",
			image: "7cups.jpeg",
		},
		{
			name: "Eight of Cups",
			meaning: "Walking Away, Disillusionmnet, Leaving Behind",
			image: "8cups.jpeg",
		},
		{
			name: "Nine of Cups",
			meaning: "Satisfaction, Emotional Stability, Luxury",
			image: "9cups.jpeg",
		},
		{
			name: "Ten of Cups",
			meaning: "Inner Happiness, Fufillment, Dreams Coming True",
			image: "10cups.jpeg",
		},
		{
			name: "King of Cups",
			meaning: "Compassion, Control, Balance",
			image: "kingcups.jpeg",
		},
		{
			name: "Knight Of Cups",
			meaning: "Following the Heart, Idealism, Romance",
			image: "knightcups.jpeg",
		},
		{
			name: "Page of Cups",
			meaning: "Suprises, Dreaming, Sensitivity",
			image: "pagecups.jpeg",
		},
		{
			name: "Queen of Cups",
			meaning: "Compassion, Calm, Comfort",
			image: "queencups.jpeg",
		},
	}
	return deck
}

func swords() (deck []card) {
	deck = []card {
		{
			name: "Ace of Swords",
			meaning: "Breakthrough, Clarity, Sharpness of Mind",
			image: "aceswords.jpeg",
		},
		{
			name: "Two of Swords",
			meaning: "Indecision, Stalemates, Difficult Choices",
			image: "2swords.jpeg",
		},
		{
			name: "Three of Swords",
			meaning: "Heartbreak, Suffering, Grief",
			image: "3swords.jpeg",
		},
		{
			name: "Four of Swords",
			meaning: "Rest, Restoration, Contemplation",
			image: "4swords.jpeg",
		},
		{
			name: "Five of Swords",
			meaning: "Ambition, Victory, Deceit",
			image: "5swords.jpeg",
		},
		{
			name: "Six of Swords",
			meaning: "Transition, Moving On, Leaving Behind",
			image: "6swords.jpeg",
		},
		{
			name: "Seven of Swords",
			meaning: "Deception, Trickery, Tactics and Strategy",
			image: "7swords.jpeg",
		},
		{
			name: "Eight of Swords",
			meaning: "Imprisonment, Entrapment",
			image: "8swords.jpeg",
		},
		{
			name: "Nine of Swords",
			meaning: "Anxiety, Hopelessness, Trauma",
			image: "9swords.jpeg",
		},
		{
			name: "Ten of Swords",
			meaning: "Failure, Collapse, Defeat",
			image: "10swords.jpeg",
		},
		{
			name: "King of Swords",
			meaning: "Discipline, Truth, Logic",
			image: "kingswords.jpeg",
		},
		{
			name: "Knight Of Swords",
			meaning: "Action, Impulse, Belief",
			image: "knightswords.jpeg",
		},
		{
			name: "Page of Swords",
			meaning: "Curiosity, Restlessness, Mental Energy",
			image: "pageswords.jpeg",
		},
		{
			name: "Queen of Swords",
			meaning: "Complexity, Perceptivness, Clear Mind",
			image: "queenswords.jpeg",
		},
	}
	return deck
}


func pentacles() (deck []card) {
	deck = []card {
		{
			name: "Ace of Pentacles",
			meaning: "Opportunity, Prosperity, New Ventures",
			image: "acepentacles.jpeg",
		},
		{
			name: "Two of Pentacles",
			meaning: "Balance, Proritising, Adapting to Change",
			image: "2pentacles.jpeg",
		},
		{
			name: "Three of Pentacles",
			meaning: "Teamwork, Collaboration, Building",
			image: "3pentacles.jpeg",
		},
		{
			name: "Four of Pentacles",
			meaning: "Conservation, Frugality, Security",
			image: "4pentacles.jpeg",
		},
		{
			name: "Five of Pentacles",
			meaning: "Need, Poverty, Insecurity",
			image: "5pentacles.jpeg",
		},
		{
			name: "Six of Pentacles",
			meaning: "Charity, Generosity, Giving",
			image: "6pentacles.jpeg",
		},
		{
			name: "Seven of Pentacles",
			meaning: "Hard Work, Perseverance, Diligence",
			image: "7pentacles.jpeg",
		},
		{
			name: "Eight of Pentacles",
			meaning: "Apprenticeship, Passion, High Standards",
			image: "8pentacles.jpeg",
		},
		{
			name: "Nine of Pentacles",
			meaning: "Rewards, Luxury",
			image: "9pentacles.jpeg",
		},
		{
			name: "Ten of Pentacles",
			meaning: "Legacy, Culmniation, Inheritance",
			image: "10pentacles.jpeg",
		},
		{
			name: "King of Pentacles",
			meaning: "Abundance, Prosperity, Security",
			image: "kingpentacles.jpeg",
		},
		{
			name: "Knight Of Pentacles",
			meaning: "Efficiency, Hard Work, Responsibility",
			image: "knightpentacles.jpeg",
		},
		{
			name: "Page of Pentacles",
			meaning: "Ambition, Desire, Diligence",
			image: "pagepentacles.jpeg",
		},
		{
			name: "Queen of Pentacles",
			meaning: "Practicality, Comfort, Finance",
			image: "queenpentacles.jpeg",
		},
	}
	return deck
}